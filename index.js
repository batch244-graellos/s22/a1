let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];


let friendsList = [];

function register(username) {
    if (registeredUsers.includes(username)) {
        alert("Registration failed. Username already exists!");
    } else {
        registeredUsers.push(username);
        alert("Thank you for registering!");
    }
}

function addFriend(username) {
    if (registeredUsers.indexOf(username) !== -1) {
        friendsList.push(username);
        alert(`You have added ${username} as a friend!`);
    } else {
        alert("User not found.");
    }
}

function displayFriends() {
    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        for (let i = 0; i < friendsList.length; i++) {
            console.log(friendsList[i]);
        }
    }
}

function displayNumberOfFriends() {
    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        alert(`You currently have ${friendsList.length} friends.`);
    }
}

function deleteLastFriend() {
    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.pop();
    }
}

function deleteFriend(username) {
    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        let index = friendsList.indexOf(username);
        if (index !== -1) {
            friendsList.splice(index, 1);
            alert(`You have deleted ${username} from your friends list.`);
        } else {
            alert("User not found in friends list.");
        }
    }
}